import 'package:flutter/material.dart';  
  
void main() {runApp(MyTable());}  
  
class MyTable extends StatefulWidget {  
  @override  
  _DataTableExample createState() => _DataTableExample();  
}  
  
class _DataTableExample extends State<MyTable> {  
  @override  
  Widget build(BuildContext context) {  
    return Center(  
      child: Center(
            child: DataTable(  
              columns: [  
                DataColumn(label: Text(  
                    'Mata Kuliah',  
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)  
                )),  
                DataColumn(label: Text(  
                    'Waktu',  
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)  
                )),  
                DataColumn(label: Text(  
                    'Topik',  
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)  
                )),  
              ],  
              rows: [  
                DataRow(cells: [  
                  DataCell(Text('PBP')),  
                  DataCell(Text('18/11/2021')),  
                  DataCell(Text('Lab 6')),  
                ]),  
                DataRow(cells: [  
                  DataCell(Text('PBP')),  
                  DataCell(Text('18/11/2021')),  
                  DataCell(Text('Lab 6')),  
                ]),  
                DataRow(cells: [  
                  DataCell(Text('PBP')),  
                  DataCell(Text('18/11/2021')),  
                  DataCell(Text('Lab 6')),  
                ]),  
                DataRow(cells: [  
                  DataCell(Text('PBP')),  
                  DataCell(Text('18/11/2021')),  
                  DataCell(Text('Lab 6')),  
                ]),  
              ],  
            ),  
           
      ),  
    );  
  }  
}  