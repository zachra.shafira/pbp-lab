/// Flutter code sample for Card

// This sample shows creation of a [Card] widget that can be tapped. When
// tapped this [Card]'s [InkWell] displays an "ink splash" that fills the
// entire card.

import 'package:flutter/material.dart';

class CardPBP extends StatefulWidget {
  const CardPBP({Key? key}) : super(key: key);

  @override
  _CardPBPState createState() => _CardPBPState();
}

class _CardPBPState extends State<CardPBP> {
  @override
  Widget build(BuildContext context) {
    return Center(

      child: Card(
        child: InkWell(
          splashColor: Colors.blue.withAlpha(30),
          child: SizedBox(
            width: 300,
            child: Column(
              children: [
                Padding(
                    padding: EdgeInsets.all(20.0),
                child:
                  Column(
                    children: [
                      SizedBox(height: 20,),
                      Text('PBP',
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                        ),
                      textAlign: TextAlign.center,),
                      SizedBox(height: 20,),
                      Text("Lab 6 PBP", style: TextStyle(
                        fontSize: 14,
                      ),),
                      SizedBox(height: 20,),
 
                        Text("Lab ini membahas mengenai Flutter", style: TextStyle(
                          fontSize: 12,
                          
                        ), textAlign: TextAlign.center, ),

                      SizedBox(height: 20,),
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(textStyle: const TextStyle(fontSize: 20),
                          primary: Colors.lightBlue[700],
                          onPrimary: Colors.white,
                          side: BorderSide(width: 2, color: Colors.blue),
                          padding: EdgeInsets.only(left: 12, right: 12, top: 8, bottom: 8),
                          shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(8.0)),),
                        onPressed: () {},
                        child: const Text('Ikut Pertemuan'),
                      ),
                    ]
                  ),),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
