from django.urls import path
from .views import index, friend_list # Menambahkan import fungsi yang digunakan yaitu index & friend_list

urlpatterns = [
    path('', index, name = 'index'),
    path('friends', friend_list, name = 'friend_list'), # Menambahkan web path yang menunjukkan list teman dari fungsi friend_list

    # TODO Add friends path using friend_list Views
]
