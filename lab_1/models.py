from django.db import models

# TODO Create Friend model that contains name, npm, and DOB (date of birth) here

class Friend(models.Model):
    def __str__(self): # Agar dapat terlihat list nama teman pada web page
        return self.name

    name = models.CharField(max_length = 30) # Variabel name berisi string field untuk nama teman dengan max string length sebesar 30
    npm = models.IntegerField() # Variabel npm berisi integer field untuk npm teman
    dob = models.DateField(max_length = 8) # Variabel dob berisi date field untuk tanggal lahir teman dengan max date length sebesar 8

    # TODO Implement missing attributes in Friend model
