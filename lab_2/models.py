from django.db import models

# Create your models here.
class Note(models.Model):
    def __str__(self): # Untuk menampilkan title dari tiap note pada halaman sebagai list
        return self.Title 

    To = models.CharField(max_length=500) # Men-assign isi dari CharField dengan panjang maksimal 500 karakter ke variabel To
    From = models.CharField(max_length=500) # Men-assign isi dari CharField dengan panjang maksimal 500 karakter ke variabel From
    Title = models.CharField(max_length=500) # Men-assign isi dari CharField dengan panjang maksimal 500 karakter ke variabel Title
    Message = models.TextField() # Men-assign isi dari TextField ke variabel Message