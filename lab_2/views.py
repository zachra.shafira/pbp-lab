from django.shortcuts import render
from .models import Note
from django.http.response import HttpResponse 
from django.core import serializers

# Create your views here.
def index(request): # Untuk menampilkan atribut note sesuai value yang dimasukkan pada halaman admin
    notes = Note.objects.all().values() # Men-assign QuerySet berisi object Note ke variabel notes
    response = {'note' : notes}
    return render(request, 'lab2.html', response) # Render untuk memuat isi konteks

def xml(request): # Untuk menampilkan halaman XML sesuai isi note
    data = serializers.serialize('xml', Note.objects.all()) # Translate model Note ke xml
    return HttpResponse(data, content_type="application/xml") # Mengembalikan objek HttpResponse, melakukan redirecting, menampilkan response berupa data

def json(request): # Untuk menampilkan halaman JSON sesuai isi note
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json")
