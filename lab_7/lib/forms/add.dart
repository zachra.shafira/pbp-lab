import 'package:flutter/material.dart';

class FormAdd extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return FormAddState();
  }
}

class FormAddState extends State<FormAdd> {
  String? _prioritas = '';
  String? _matkul = '';
  String? _waktu = '';
  String? _topik = '';
  String? _info = '';
  String? _situs = '';

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  Widget _buildPrioritas() {
    return TextFormField(
      decoration: InputDecoration(
        hintText: "Tentukan prioritas Jadwal Belajar Bareng.",
        labelText: "Prioritas",
        border:
            OutlineInputBorder(borderRadius: new BorderRadius.circular(5.0)),
      ),
      keyboardType: TextInputType.url,
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Prioritas tidak boleh kosong.';
        }
        return null;
      },
      onSaved: (String? value) {
        _prioritas = value!;
      },
    );
  }

  Widget _buildMatkul() {
    return TextFormField(
      decoration: InputDecoration(
        hintText: "Masukkan mata kuliah Jadwal Belajar Bareng.",
        labelText: "Mata Kuliah",
        border:
            OutlineInputBorder(borderRadius: new BorderRadius.circular(5.0)),
      ),
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Mata kuliah tidak boleh kosong';
        }
        return null;
      },
      onSaved: (String? value) {
        _matkul = value!;
      },
    );
  }

  Widget _buildWaktu() {
    return TextFormField(
      decoration: InputDecoration(
        hintText: "Tentukan waktu pelaksanaan Jadwal Belajar Bareng.",
        labelText: "Waktu",
        border:
            OutlineInputBorder(borderRadius: new BorderRadius.circular(5.0)),
      ),
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Waktu tidak boleh kosong';
        }
        return null;
      },
      onSaved: (String? value) {
        _waktu = value!;
      },
    );
  }

  Widget _buildTopik() {
    return TextFormField(
      decoration: InputDecoration(
        hintText: "Masukkan topik pembelajaran.",
        labelText: "Topik",
        border:
            OutlineInputBorder(borderRadius: new BorderRadius.circular(5.0)),
      ),
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Topik tidak boleh kosong';
        }
        return null;
      },
      onSaved: (String? value) {
        _topik = value!;
      },
    );
  }
  Widget _buildInfo() {
    return TextFormField(
      decoration: InputDecoration(
        hintText: "Masukkan informasi tambahan.",
        labelText: "Informasi",
        border:
            OutlineInputBorder(borderRadius: new BorderRadius.circular(5.0)),
      ),
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Informasi tidak boleh kosong';
        }
        return null;
      },
      onSaved: (String? value) {
        _info = value!;
      },
    );
  }
  Widget _buildSitus() {
    return TextFormField(
      decoration: InputDecoration(
        hintText: "Masukkan situs pertemuan daring.",
        labelText: "Situs",
        border:
            OutlineInputBorder(borderRadius: new BorderRadius.circular(5.0)),
      ),
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Situs tidak boleh kosong';
        }
        return null;
      },
      onSaved: (String? value) {
        _situs = value!;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(24),
      child: Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            _buildPrioritas(),
            SizedBox(height: 50),
            _buildMatkul(),
            SizedBox(height: 50),
            _buildWaktu(),
            SizedBox(height: 50),
            _buildTopik(),
            SizedBox(height: 50),
            _buildInfo(),
            SizedBox(height: 50),
            _buildSitus(),
            SizedBox(height: 50),
            MaterialButton(
              color: Color.fromARGB(255, 11, 94, 215),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(5.0))),
              child: Text(
                'Buat Jadwal',
                style: TextStyle(color: Colors.white, fontSize: 15),
              ),
              padding:
                  const EdgeInsets.symmetric(vertical: 20.0, horizontal: 20.0),
              onPressed: () {
                if (!_formKey.currentState!.validate()) {
                  return;
                }
                _formKey.currentState!.save();
                print(_prioritas);
                print(_matkul);
                print(_waktu);
                print(_topik);
                print(_info);
                print(_situs);

                ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(content: Text('Jadwal berhasil ditambahkan.')),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
