import 'package:flutter/material.dart';
import 'package:lab_7/screens/drawer_screen.dart';
import 'add.dart';

class FormAddPage extends StatelessWidget {
  const FormAddPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
          children: [
            SizedBox(
              height: 40,
            ),
            Text(
              "Buat Jadwal",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 30,
                  fontWeight: FontWeight.bold),
            ),
            FormAdd()
          ],
        ),

    );
  }
}
