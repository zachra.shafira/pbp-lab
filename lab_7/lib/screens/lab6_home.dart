
import 'package:flutter/material.dart';
import 'package:lab_7/main.dart';
import 'package:lab_7/widgets/card.dart';
import 'package:lab_7/widgets/image.dart';
import 'package:lab_7/widgets/search.dart';
import 'package:lab_7/widgets/table.dart';
import 'package:lab_7/widgets/title_card.dart';
import 'package:lab_7/widgets/title_table.dart';
import 'package:lab_7/forms/form.dart';

class Lab6Home extends StatefulWidget {
  const Lab6Home({Key? key}) : super(key: key);

  @override
  _Lab6HomeState createState() => _Lab6HomeState();
}

class _Lab6HomeState extends State<Lab6Home> {
  @override
  Widget build(BuildContext context) {
    return Column(
        // mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
        MyGif(),
        FormAddPage(),
        SizedBox(height: 40,),
        TitleCard(),
        SizedBox(height: 20,),
        CardPBP(),
        SizedBox(height: 5,),
        CardPBP(),
        SizedBox(height: 5,),
        CardPBP(),
        SizedBox(height: 5,),
        CardPBP(),
        SizedBox(height: 40,),
        TitleTable(),
        SearchBar(),
        MyTable(),
        SizedBox(height: 40,),

        ]
    );
  }
}
