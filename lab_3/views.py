from django.shortcuts import render

# Create your views here.

from django.http.response import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from lab_1.models import Friend
from lab_3.forms import FriendForm

@login_required(login_url='/admin/login/') # Untuk mengakses halaman, harus merupakan admin (login required), melimitasi akses hanya pada pengguna yang memiliki akun

def index(request): # Untuk menampilkan atribut Friend sesuai value yang dimasukkan
    friends = Friend.objects.all().values() # Men-assign QuerySet berisi object Friend ke variabel friends
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response) # Render untuk memuat isi konteks

@login_required(login_url='/admin/login/')

def add_friend(request):
    form = FriendForm()
    if request.method == 'POST': # Apabila request method-nya POST
        form = FriendForm(request.POST) # Dipakai untuk melakukan request POST (men-submit data)
        if form.is_valid(): # Save jika form valid
            form.save()
            return HttpResponseRedirect('/lab-3') # Melakukan redirecting ke path /lab-3

    response = {'form' : form}
    return render(request, 'lab3_form.html', response) # Balik ke halaman form


