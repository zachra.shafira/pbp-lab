from django import forms
from lab_1.models import Friend
from django.forms import widgets

class DateInput(forms.DateInput):
    input_type = 'date'

class FriendForm(forms.ModelForm): # Membuat class FriendForm
    class Meta: # Men-assign model pada class Meta dengan Friend model dari lab_1/models.py
        model = Friend
        fields = "__all__"
        widgets = {'dob': DateInput()} # DateInput agar user dapat melakukan input dob dengan menggunakan kalender