from django.urls import path
from .views import add_friend, index

urlpatterns = [
    path('', index, name='index'), # Path halaman utama lab-3
    path('add', add_friend, name='add_friend'), # Path add untuk menambah Friend
]
