1. Apakah perbedaan antara JSON dan XML?

    JSON dan XML, keduanya merupakan format umum pertukaran data dari berbagai sistem pada situs. Sebagian besar JSON dipakai untuk Asynchronous JavaScript (AJAX) serta pengembangan layanan web RESTful. XML digunakan untuk membuat format informasi umum serta menjadi sarana untuk membagikan format dan data yang digunakan di World Wide Web, intranet, dan di platform lain yang menggunakan teks ASCII standar. JSON dan XML memiliki beberapa perbedaan, diantaranya:

    JSON (JavaScript Object Notation): 
    - Format ekstensi file JSON adalah .json
    - Data oriented
    - Lebih sederhana dalam pembacaan dan penulisannya dibandingkan XML
    - Merupakan bahasa meta yang dapat digunakan sebagai cara yang efisien untuk merepresentasikan data secara terstruktur
    
    XML (eXtensible Markup Language): 
    - Format ekstensi file XML adalah .xml
    - Document oriented
    - Lebih rumit dan sulit dalam pembacaan dan penulisannya dibandingkan JSON
    - Merupakan bahasa markup yang digunakan untuk merepresentasikan item dari sebuah data


2. Apakah perbedaan antara HTML dan XML?

    HTML :
    - Format ekstensi file HTML adalah .html
    - Fokus pada penyajian data
    - Mementingkan format penyajian
    - Case insensitive
    - Fokus pada format dari tampilan data

    XML :
    - Format ekstensi file xml adalah .xml
    - Fokus pada transfer data
    - Mementingkan konten
    - Case sensitive
    - Fokus pada struktur dan konteks


Referensi:


Nastainullah, R. (2020, October 23). Apa Saja Perbedaan XML dan HTML. Retrieved from Masterweb: https://blogs.masterweb.com/perbedaan-xml-dan-html/#Perbedaan_XML_dan_HTML

Oliver, A. (2021, April 19). Ketahui Apa itu XML dan Perannya untuk Pemrograman. Retrieved from Glints: https://glints.com/id/lowongan/xml-adalah/#.YVBof7gzZnI

Perbedaan Antara JSON dan XML. (n.d.). Retrieved from Sawakinome: https://id.sawakinome.com/articles/technology/difference-between-json-and-xml-2.html
