from lab_2.models import Note
from django import forms

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note # Men-assign model pada class Meta dengan model Note dari lab_2/models.py
        fields = "__all__" # Menggunakan seluruh field dari model Note