from .views import index, add_note, note_list
from django.urls import path

urlpatterns = [
    path('', index, name='index'), # Path halaman utama lab-4
    path('home', index, name='index'), # Path halaman home lab-3, mengarahkan ke halaman yang sama seperti line 5
    path('add-note', add_note, name='add-note'), # Path add-note
    path('note-list', note_list, name='note-list'), # Path note-list
]
