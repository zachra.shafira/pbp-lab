from django.shortcuts import render, redirect
from .forms import NoteForm
from lab_2.models import Note


# Create your views here.
def index(request): # Untuk menampilkan atribut Note sesuai value yang dimasukkan
    notes = Note.objects.all().values() # Men-assign QuerySet berisi object Friend ke variabel friends
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response) # Render untuk memuat isi konteks

def add_note(request):
    form = NoteForm
    if request.method == 'POST':  # Apabila request method-nya POST
        noteForm = NoteForm(request.POST) # Dipakai untuk melakukan request POST (men-submit data)
        if noteForm.is_valid(): # Save jika form valid
            noteForm.save()
            return redirect('/lab-4') # Mengarahkan ke path /lab-4

    response = {'form' : form}
    return render(request, 'lab4_form.html', response) # Balik ke halaman form, gak berhasil di save

def note_list(request): 
    notes = Note.objects.all().values()
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response) # Memuat isi konteks
